Django==1.10
django-emoji==2.2.0
Fabric==1.12.0
gunicorn==19.6.0
